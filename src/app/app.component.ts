import { APP_INITIALIZER, FactoryProvider } from "@angular/core";
import { tap } from "rxjs/operators";
import { ConfigService } from './app-initializer/config.service';
import { Component } from '@angular/core';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'test-ci';
}
