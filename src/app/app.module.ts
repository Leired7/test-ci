import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

import { AppComponent } from './app.component';
import { loadConfigProvider } from './app-initializer/app.initializer'

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule, HttpClientModule
  ],
  providers: [
    loadConfigProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
