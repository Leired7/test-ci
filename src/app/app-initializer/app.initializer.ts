import { APP_INITIALIZER, FactoryProvider } from "@angular/core";
import { tap } from "rxjs/operators";

import { ConfigService } from "./config.service";

function loadConfigFactory(configService: ConfigService) {
  // Easy as pie 🥧
  return () => configService.getConfig().pipe(tap(value => console.log(value))); // 👈

  // How you might've done it "before"
  // return () => configService.getConfig().toPromise();
}

export const loadConfigProvider: FactoryProvider = {
  provide: APP_INITIALIZER,
  useFactory: loadConfigFactory,
  deps: [ConfigService],
  multi: true
};
