import { Injectable } from "@angular/core";

import { of } from "rxjs";
import { delay } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class ConfigService {
  getConfig() {
    return of({ dashboardEnabled: true }).pipe(delay(2000));
  }
}
